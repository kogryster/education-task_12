# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EKATERINA GERASIMOVA

**E-MAIL**: kogryster@gmail.com

# SOFTWARE

- JDK 1.8
- MS WINDOWS 7 / MS WINDOWS 10

# PROGRAM BUILD

```bash
mvn clean instal
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOT

**JSE-11**: https://yadi.sk/d/u_vVIAfJcONbYQ?w=1

**JSE-12**: https://yadi.sk/d/dY9L13rYuIYBdg?w=1
package ru.gerasimova.tm.api.service;

import ru.gerasimova.tm.model.Project;
import ru.gerasimova.tm.model.Task;

import java.util.List;

public interface IProjectService {
    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateProjectById(String id, String name, String discription);

    Project updateTaskByIndex(Integer index, String name, String description);

}

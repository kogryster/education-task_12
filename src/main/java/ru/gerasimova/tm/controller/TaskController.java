package ru.gerasimova.tm.controller;


import ru.gerasimova.tm.api.controller.ITaskController;
import ru.gerasimova.tm.api.service.ITaskService;
import ru.gerasimova.tm.model.Task;
import ru.gerasimova.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
            System.out.println("[SHOW TASK]");
            System.out.println("[ENTER ID:]");
            final String id = TerminalUtil.nextLine();
            final Task task = taskService.findOneById(id);
            if (task == null) {
                System.out.println("[FAIL]");
                return;
            }
            showTask(task);
            System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
            System.out.println("[UPDATE TASK]");
            System.out.println("[ENTER INDEX:]");
            final Integer index = TerminalUtil.nextNumber() -1;
            final Task task = taskService.findOneByIndex(index);
            if (task == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[ENTER NAME]");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
            if (taskUpdated == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[OK]");
    }


    @Override
    public void updateTaskById() {
            System.out.println("[UPDATE TASK]");
            System.out.println("[ENTER ID:]");
            final String id = TerminalUtil.nextLine();
            final Task task = taskService.findOneById(id);
            if (task == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[ENTER NAME]");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Task taskUpdated = taskService.updateTaskById(id, name, description);
            if (taskUpdated == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
package ru.gerasimova.tm.controller;

import ru.gerasimova.tm.api.controller.IProjectController;
import ru.gerasimova.tm.api.service.IProjectService;
import ru.gerasimova.tm.model.Project;
import ru.gerasimova.tm.model.Task;
import ru.gerasimova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
            System.out.println("[REMOVE PROJECT]");
            System.out.println("[ENTER INDEX:]");
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.removeOneByIndex(index);
            if (project == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
            System.out.println("[REMOVE PROJECT]");
            System.out.println("[ENTER NAME:]");
            final String name = TerminalUtil.nextLine();
            final Project project  = projectService.removeOneByName(name);
            if (project == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
            System.out.println("[SHOW PROJECT]");
            System.out.println("[ENTER ID:]");
            final String id = TerminalUtil.nextLine();
            final Project project = projectService.findOneById(id);
            if (project == null) {
                System.out.println("[FAIL]");
                return;
            }
            showProject(project);
            System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
            System.out.println("[UPDATE PROJECT]");
            System.out.println("[ENTER INDEX:]");
            final Integer index = TerminalUtil.nextNumber();
            final Project project = projectService.findOneByIndex(index);
            if (project == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[ENTER NAME]");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Project projectUpdated = projectService.updateTaskByIndex(index, name, description);
            if (projectUpdated == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[OK]");
        }

        @Override
    public void updateProjectById() {
            System.out.println("[UPDATE PROJECT]");
            System.out.println("[ENTER ID:]");
            final String id = TerminalUtil.nextLine();
            final Project project = projectService.findOneById(id);
            if (project == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[ENTER NAME]");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Project projectUpdated = projectService.updateProjectById(id, name, description);
            if (projectUpdated == null) {
                System.out.println("[FAIL]");
                return;
            }
            System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
            System.out.println("[REMOVE PROJECT]");
            System.out.println("[ENTER ID:]");
            final String id = TerminalUtil.nextLine();
            final Project project = projectService.removeOneById(id);
            if (project == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
    }

}